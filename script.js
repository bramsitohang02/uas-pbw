// menu default
$(document).ready(function () {
    let daftarMenu = [{
            "nama": "Kopi Hitam"
        },
        {
            "nama": "Kopi Susu"
        },
        {
            "nama": "Kopi Tubruk"
        },
        {
            "nama": "Cappuccino"
        },
        {
            "nama": "Moccacino"
        },
        {
            "nama": "Teh Manis"
        },
        {
            "nama": "Teh Tawar"
        },
    ];
    let daftarPesanan = [];
    showMenu(daftarMenu);
    showOrder(daftarPesanan);
    // 

// btn tambah menu
    $('.btn-tambah-menu').on('click', function(){
        daftarMenu.push({
            "nama" : $('#tambah_menu').val()
        });
        showMenu(daftarMenu);
        $('#tambah_menu').val('');
        $('#tambahMenuModal').modal('hide');
    })
    //
     
// btn pesan sekarang
    $('#pesanSekarang').on('click', function(){
        daftarPesanan.push({
            "menu" : $('#menu').val(),
            "jumlah" : $('#jumlah').val()
        });
        $('#menu').val('')
        $('#jumlah').val('')
        $('.notifikasi').fadeIn();
        $('.notifikasi').fadeOut(3000);
        showOrder(daftarPesanan);
    })
    // 

// btn hapus daftar pesanan
    $('#daftarPesanan').on('click', '.btn-hapus' , function(){
        daftarPesanan.splice($(this).data('id'), 1)
        showOrder(daftarPesanan);
    })
    // 

// btn edit daftar pesanan
    $('#daftarPesanan').on('click', '.btn-edit' , function(){
        $('#id_pesanan').val($(this).data('id'));
        $('#edit_jumlah').val(daftarPesanan[$(this).data('id')].jumlah);
        $('#editModal').modal('show');
        showOrder(daftarPesanan);
    })
    // 

// btn update daftar pesanan
    $('.btn-update').on('click', function(){        
        daftarPesanan[$('#id_pesanan').val()].menu = $('#edit_menu').val();
        daftarPesanan[$('#id_pesanan').val()].jumlah = $('#edit_jumlah').val();
        $('#editModal').modal('hide');
        showOrder(daftarPesanan);
    })
});
    // 

// tampilan daftar menu (js)
function showMenu(daftarMenu) {
    $('#daftarMenu').empty();
    $('#menu, #edit_menu').empty();
    $.each(daftarMenu, function (i, data) {
        let no = i + 1;
        $('#daftarMenu').append(`
                <tr>
                    <th scope="row">${no}</th>
                    <td>${data.nama}</td>
                </tr>
            `)
        $('#menu, #edit_menu').append(`
                <option value="${data.nama}">${data.nama}</option>
            `)
    })
}

// tampilan daftar pesanan (js)
function showOrder(daftarPesanan){
    $('#daftarPesanan').empty();
    $.each(daftarPesanan, function (i, data) {
        $('#daftarPesanan').append(`
            <tr>
                <td>${data.menu}</td>
                <td>${data.jumlah}</td>
                <td>
                    <a href="#!" class="btn btn-sm btn-warning btn-edit" data-id="${i}">Edit</a>
                    <a href="#!" class="btn btn-sm btn-danger btn-hapus" data-id="${i}">Hapus</a>
                </td>
            </tr>
            `)
    })
}